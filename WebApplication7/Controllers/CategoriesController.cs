﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7;

namespace WebApplication7.Controllers
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        private NotedDBEntities db = new NotedDBEntities();

        // GET: api/Categories
        public IEnumerable<TempCategory> Get()
        {

            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            List<TempCategory> temp = new List<TempCategory>();
            foreach (var item in db.Categories.ToList())
            {
                if (item.AspNetUserId == user.Id)
                    temp.Add(new TempCategory { Id = item.Id, AspNetUserEmail = item.AspNetUser.Email, Text = item.Text, notesCount = item.Notes.Count, tabsCount = item.Tabs.Count });
            }
            return temp;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(TempCategory))]
        public IHttpActionResult GetCategory(int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(new TempCategory { Id = category.Id, AspNetUserEmail = category.AspNetUser.Email, Text = category.Text, notesCount = category.Notes.Count, tabsCount = category.Tabs.Count });
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(int id, TempCategory category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Category categoryUpdate = db.Categories.Find(id);
            categoryUpdate.Text = category.Text;
            db.Entry(categoryUpdate).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.OK);
        }

        // POST: api/Categories
        [ResponseType(typeof(TempCategory))]
        public IHttpActionResult PostCategory([FromBody]TempCategory category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            Category newCategory = new Category() { AspNetUserId = user.Id, Text = category.Text, AspNetUser = user };
            db.Categories.Add(newCategory);

            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = newCategory.Id }, new TempCategory { Id = newCategory.Id, AspNetUserEmail = newCategory.AspNetUser.Email, Text = category.Text, notesCount = 0, tabsCount = 0 });
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult DeleteCategory(int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            category.Notes.Clear();
            category.Tabs.Clear();
            db.Categories.Remove(category);
            db.SaveChanges();

            return Ok(category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.Id == id) > 0;
        }
    }

    public class TempCategory
    {
        public int Id { get; set; }
        public string AspNetUserEmail { get; set; }
        public string Text { get; set; }
        public int notesCount { get; set; }
        public int tabsCount { get; set; }
    }
}
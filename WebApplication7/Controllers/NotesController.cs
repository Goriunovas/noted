﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7;
using System.Globalization;

namespace WebApplication7.Controllers
{
    [Authorize]
    public class NotesController : ApiController
    {
        private NotedDBEntities db = new NotedDBEntities();

        public IEnumerable<ASP_Note> Get()
        {
            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            List<ASP_Note> temp = new List<ASP_Note>();
            foreach (var item in db.Notes.ToList())
            {
                if (item.AspNetUserId == user.Id)
                    temp.Add(new ASP_Note(item.Id, item.AspNetUser.Email, item.Name, item.Text, item.CreationDate, item.Dates, item.Categories));
            }
            return temp;
        }

        // GET: api/Notes/5
        [ResponseType(typeof(ASP_Note))]
        public IHttpActionResult GetNote(int id)
        {
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            return Ok(new ASP_Note(note.Id, note.AspNetUser.Email, note.Name, note.Text, note.CreationDate, note.Dates, note.Categories));
        }

        // PUT: api/Notes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNote(int id, Client_Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            Note noteUpdate = db.Notes.Find(id);
            noteUpdate.Name = note.Name;
            noteUpdate.Text = note.Text;
            noteUpdate.Categories.Clear();
            db.Dates.RemoveRange(noteUpdate.Dates);

            if (note.categories != null)
            {
                foreach (var category in note.categories)
                {
                    noteUpdate.Categories.Add(db.Categories.First(e => e.Id == category.Id));
                }
            }

            if (note.Dates != null)
            {
                foreach (var date in note.Dates)
                {
                    DateTime dt = DateTime.Today;
                    if (date.DateTime != "undefined" && date.DateTime.Length > 0)
                        dt = DateTime.ParseExact(date.DateTime,
                                       "yyyy-MM-dd'T'HH:mm:ss.fff'Z'",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.AssumeUniversal |
                                       DateTimeStyles.AdjustToUniversal);

                    Date newDate = new Date() { DateTime = dt, IsReminder = date.IsReminder, RepeatTime = date.RepeatTime, Note = noteUpdate, NoteId = noteUpdate.Id };
                    db.Dates.Add(newDate);
                    noteUpdate.Dates.Add(newDate);
                }
            }

            db.Entry(noteUpdate).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Notes
        [ResponseType(typeof(Client_Note))]
        public IHttpActionResult PostNote(Client_Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            Note newNote = new Note() { AspNetUserId = user.Id, Name = note.Name, Text = note.Text, CreationDate = DateTime.Today, AspNetUser = user };
            db.Notes.Add(newNote);

            if (note.categories != null)
            {
                foreach (var category in note.categories)
                {
                    newNote.Categories.Add(db.Categories.First(e => e.Id == category.Id));
                }
            }

            if (note.Dates != null)
            {
                foreach (var date in note.Dates)
                {
                    DateTime dt = DateTime.Today;
                    if (date.DateTime != "undefined" && date.DateTime.Length > 0)
                        dt = DateTime.ParseExact(date.DateTime.Substring(0, 24),
                                      "ddd MMM d yyyy HH:mm:ss",
                                      CultureInfo.InvariantCulture);

                    Date newDate = new Date() { DateTime = dt, IsReminder = date.IsReminder, RepeatTime = date.RepeatTime, Note = newNote, NoteId = newNote.Id };
                    db.Dates.Add(newDate);
                    newNote.Dates.Add(newDate);
                }
            }

            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = newNote.Id }, new ASP_Note(newNote.Id, newNote.AspNetUser.Email, newNote.Name, newNote.Text, newNote.CreationDate, newNote.Dates, newNote.Categories));
        }

        // DELETE: api/Notes/5
        [ResponseType(typeof(ASP_Note))]
        public IHttpActionResult DeleteNote(int id)
        {
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            note.Categories.Clear();
            db.Dates.RemoveRange(note.Dates);
            db.Notes.Remove(note);
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoteExists(int id)
        {
            return db.Notes.Count(e => e.Id == id) > 0;
        }
    }

    public class ASP_Note
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
        public List<Client_Date> Dates { get; set; }
        public List<TempCategory> categories { get; set; }

        public ASP_Note(int Id, string Email, string Name, string Text, DateTime CreationDate, ICollection<Date> Dates, ICollection<Category> categories)
        {
            this.Id = Id;
            this.Email = Email;
            this.Name = Name;
            this.Text = Text;
            this.CreationDate = CreationDate;
            this.categories = new List<TempCategory>();
            this.Dates = new List<Client_Date>();
            foreach (var category in categories)
            {
                this.categories.Add(new TempCategory { Id = category.Id, AspNetUserEmail = category.AspNetUser.Email, Text = category.Text, notesCount = category.Notes.Count, tabsCount = category.Tabs.Count });
            }
            foreach (var date in Dates)
            {
                this.Dates.Add(new Client_Date { DateTime = date.DateTime.ToString("yyyy-MM-ddTHH:mm:ss"), IsReminder = date.IsReminder, RepeatTime = date.RepeatTime });
                
            }
        }
    }

    public class Client_Note
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public List<Client_Date> Dates { get; set; }
        public List<TempCategory> categories { get; set; }
    }

    public class Client_Date
    {
        public string DateTime { get; set; }
        public bool IsReminder { get; set; }
        public int RepeatTime { get; set; }
        public int Repeat { get; set; }
    }
}
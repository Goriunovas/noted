﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7;

namespace WebApplication7.Controllers
{
    [Authorize]
    public class TabsController : ApiController
    {
        private NotedDBEntities db = new NotedDBEntities();

        // GET: api/Notes1
        public IEnumerable<TempTab> Get()
        {
            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            List<TempTab> temp = new List<TempTab>();
            foreach (var item in db.Tabs.ToList())
            {
                if (item.AspNetUserId == user.Id)
                    temp.Add(new TempTab(item.Id, item.AspNetUser.Email, item.Name, item.Categories));
            }
            return temp;
        }

        // GET: api/Notes1/5
        [ResponseType(typeof(Note))]
        public IHttpActionResult GetTab(int id)
        {
            Tab note = db.Tabs.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            return Ok(new TempTab(note.Id, note.AspNetUser.Email, note.Name, note.Categories));
        }

        // PUT: api/Notes1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTab(int id, TempTestTab tab)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Tab tabUpdate = db.Tabs.Find(id);
            tabUpdate.Name = tab.Name;

            tabUpdate.Categories.Clear();
            if (tab.categories != null)
            {
                foreach (var category in tab.categories)
                {
                    tabUpdate.Categories.Add(db.Categories.First(e => e.Id == category.Id));
                }
            }

            db.Entry(tabUpdate).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Notes1
        [ResponseType(typeof(TempTab))]
        public IHttpActionResult PostNote([FromBody]TempTestTab tab)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            AspNetUser user = db.AspNetUsers.First(e => e.Email == User.Identity.Name);
            Tab newTab = new Tab() { AspNetUserId = user.Id, Name = tab.Name, AspNetUser = user };

            if (tab.categories != null)
            {
                foreach (var category in tab.categories)
                {
                    newTab.Categories.Add(db.Categories.First(e => e.Id == category.Id));
                }
            }

            db.Tabs.Add(newTab);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = newTab.Id }, new TempTab(newTab.Id, newTab.AspNetUser.Email, newTab.Name, newTab.Categories));
        }

        // DELETE: api/Notes1/5
        [ResponseType(typeof(Note))]
        public IHttpActionResult DeleteTab(int id)
        {
            Tab tab = db.Tabs.Find(id);
            tab.Categories.Clear();

            if (tab == null)
            {
                return NotFound();
            }

            db.Tabs.Remove(tab);
            db.SaveChanges();

            return Ok(tab);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoteExists(int id)
        {
            return db.Tabs.Count(e => e.Id == id) > 0;
        }
    }

    public class TempTab
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public List<TempCategory> categories { get; set; }

        public TempTab(int Id, string Email, string Name, ICollection<Category> categories)
        {
            this.Id = Id;
            this.Email = Email;
            this.Name = Name;
            this.categories = new List<TempCategory>();

            foreach (var category in categories)
            {
                this.categories.Add(new TempCategory { Id = category.Id, AspNetUserEmail = category.AspNetUser.Email, Text = category.Text, notesCount = category.Notes.Count, tabsCount = category.Tabs.Count });
            }
        }
    }

    public class TempTestTab
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public List<TempCategory> categories { get; set; }
    }
}